<?php


namespace App\Tests\Entity;

use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class VoitureTest extends TestCase
{
    public function testVoitureEntity()
    {
        // Create a Voiture entity
        $voiture = new Voiture();

        // Set some values for testing
        $voiture->setModele('Sedan');
        $voiture->setMarche('128100');

        // Assert that getters return the correct values
        $this->assertEquals('Sedan', $voiture->getModele());
        $this->assertEquals('128100', $voiture->getMarche());

        
        $annee = '2022';
        $voiture->setDateMise($annee);
        $this->assertEquals($annee, $voiture->getDateMise());

        
    }
}

