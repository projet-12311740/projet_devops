<?php

// tests/Entity/LocationTest.php

namespace App\Tests\Entity;

use App\Entity\Location;
use App\Entity\Client;
use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{
    public function testLocationEntity()
    {
        // Create a Location entity
        $location = new Location();

        // Set some values for testing
        $dateDebutString = '2024-01-01';
        $dateRetourString = '2024-01-10';
        $prixString = '100.0';

        $location->setDateDebut($dateDebutString);
        $location->setDateRetour($dateRetourString);
        $location->setPrix($prixString);

        // Create a Client entity
        $client = new Client();
        $client->setNom('John Doe');
        $client->setCin('123456');
        $location->setClient($client);

        // Assert that getters return the correct values
        $this->assertEquals($dateDebutString, $location->getDateDebut());
        $this->assertEquals($dateRetourString, $location->getDateRetour());
        $this->assertEquals($prixString, $location->getPrix());

        

        
        $this->assertEquals($client, $location->getClient());
        $this->assertEquals('John Doe', $location->getClient()->getNom());
        $this->assertEquals('123456', $location->getClient()->getCin());

        
        $vehicule = new Voiture(); 
        $location->setVoiture($vehicule);
        $this->assertEquals($vehicule, $location->getVoiture());

        
    }
}

