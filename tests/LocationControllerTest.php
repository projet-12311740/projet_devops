<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LocationControllerTest extends WebTestCase
{
    public function testShouldDisplayLocationIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/location');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Location index');
    }

}
